
 // VARIABLES *******************************************************************

var mySplitText = new SplitText(".dreamingeffect", {type:"chars, words"}),
    tldreamtxt = new TimelineLite(),
    numChars = mySplitText.chars.length;

var tl_dreamingtitle = new TimelineLite, 
    mySplitText_dreamingtitle = new SplitText("#dreamingtitle", {type:"words,chars"}), 
    chars_dreamingtitle = mySplitText_dreamingtitle.chars; //an array of all the divs that wrap each character

var tl_dreamingsubtitle = new TimelineLite, 
    mySplitText_dreamingsubtitle = new SplitText("#dreamingsubtitle", {type:"words,chars"}), 
    chars_dreamingsubtitle = mySplitText_dreamingsubtitle.chars; //an array of all the divs that wrap each character


 // INTRO *******************************************************************



var tl_intro = new TimelineMax();

$(window).load(function() {
	
	// LOADER DISAPPEARS
	$(".loader").fadeOut("4000");
	
	// TIMELINE INTRO
	tl_intro.timeScale(1);
	tl_intro.to("#loaderback", 0, {display:"none"}, 1.0)
	tl_intro.to("#dream", 0, {display:""})
	.to("#dreamingtitle", 0, {display:""})
	.staggerFrom(chars_dreamingtitle, 0.1, {opacity:0}, 0.1)

	.to("#dreamingsubtitle", 0, {display:""}, "+=2")
	.staggerFrom(chars_dreamingsubtitle, 0.1, {opacity:0}, 0.01)

	/*
	for(var i = 0; i < numChars; i++){
			//random value used as position parameter
			tldreamtxt.from(mySplitText.chars[i], 3, {opacity:0}, Math.random() * 2);
		}
	*/
	.to("#wake", 0.5, {display:""}, "+=0.5")
	.from("#wake", 0.1, {width:0})
	.to("#wake", 0.1, {color:000000}, "+=0.5");

	$("#burger").on("click", function() {
	    $( this ).toggleClass("burger-fx");
	    $( "#top-navbar-fixed" ).toggleClass("top-navbar-fixed-menu");
	    $( "#nav" ).toggle();
   	});


})

 // TIMELINE PRINCIPALE *******************************************************************




// BOUTON SLEEP

$("#sleep").on("click", 
 function() {
    tl.timeScale(4);
    tl.reverse();
   });


