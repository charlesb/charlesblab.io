import React from 'react'
import PropTypes from 'prop-types'
import { Link, StaticQuery, graphql } from 'gatsby'

import Header from './header'
import './layout.css'

const Layout = ({ children, isHome }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title,
            description
          }
        }
      }
    `}
    render={data => (
      <>
        <div className="viewport">
          <div className="viewport-top">
            <Header isHome={isHome} siteTitle={data.site.siteMetadata.title} siteDescription={data.site.siteMetadata.description} />

            <main className="site-main">
              {/* All the main content gets inserted here, index.js, post.js */}
              {children}
            </main>
          </div>
          <div className="viewport-bottom">
            {/* The footer at the very bottom of the screen */}
            <footer className="site-foot">
              <div className="site-foot-nav container">
                <div>
                  <Link to="/">{data.site.siteMetadata.title}</Link> © 2020
                </div>
              </div>
            </footer>
          </div>
        </div>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
