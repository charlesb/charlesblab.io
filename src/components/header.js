import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React from 'react'

import githubIcon from '../images/icons/github.svg'
import linkedInIcon from '../images/icons/linkedin.svg'
import envelopeIcon from '../images/icons/envelope.svg'
import backgroundHero from '../images/hero_background.jpg'

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.state = {
      menuOpen: false,
      scrolled: false
    };
  }

  handleMenuClick () {
    this.setState({menuOpen: !this.state.menuOpen});
  };


  listenScrollEvent = e => {
    if (window.scrollY > 50) {
      this.setState({scrolled: true})
    } else {
      this.setState({scrolled: false})
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.listenScrollEvent)
  }

  render() {
    const siteTitle = this.props.siteTitle;
    const isHome = this.props.isHome;
    const siteDescription = this.props.siteDescription;
    const menuOpen = this.state.menuOpen;
    
    return (
      <div>
        <header className={"site-head " + (isHome ? 'is-home' : '')} style={{ backgroundImage:  'linear-gradient(rgba(20,20,20, 1),rgba(20,20,20, .1)),url(' + backgroundHero + ')' }} >
          <div className="container">
            <div className={"site-mast " + (this.state.scrolled ? 'scrolled' : '')}>
              <div className="site-mast-left">
                <a href="https://github.com/quebecCu" className="site-nav-item"  target="_blank" rel="noopener noreferrer"><img className="site-nav-icon" src={githubIcon} alt="Github" /></a>
                <a href="https://www.linkedin.com/in/charles-belzile-0107946b/" className="site-nav-item" target="_blank" rel="noopener noreferrer"><img className="site-nav-icon" src={linkedInIcon} alt="Linkedin" /></a>
                <a href="mailto:charles.belzile@hotmail.com" className="site-nav-item" target="_blank" rel="noopener noreferrer"><img className="site-nav-icon" src={envelopeIcon} alt="Courriel" /></a>
              </div>
              <div className="site-mast-right">
                <div className={"menu-listing " + (menuOpen ? 'open' : '')}>
                  <Link className="site-nav-item" to="/" >Accueil</Link>
                  <Link className="site-nav-item" to="/#a-propos" >À propos</Link>
                  <Link className="site-nav-item" to="/#portfolio" >Portfolio</Link>
                  <Link className="site-nav-item" to="/#contact">Contact</Link>
                </div>
                <div className={"menu-hamburger " + (menuOpen ? 'open' : '')} onClick={this.handleMenuClick}>
                  <div className="bar1"></div>
                  <div className="bar2"></div>
                  <div className="bar3"></div>
                </div>
              </div>
            </div>
            { isHome ?
                <div className="site-banner-wrapper">
                  <div className="site-banner">
                    <h1 className="site-banner-title">{siteTitle}</h1>
                    <h2 className="site-banner-desc">{siteDescription}</h2>
                  </div>
                </div> :
              null}
          </div>
        </header>
      </div>
    )
  }
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
