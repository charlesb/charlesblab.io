import React from 'react'
import Img from 'gatsby-image'

import Layout from '../components/layout'
import SEO from '../components/seo'
import separator from '../images/separator.svg'
import separatorWhite from '../images/separator-white.svg'
import photshopImg from '../images/technologies/photoshop.svg'
import gatsbyImg from '../images/technologies/gatsby.svg'

const IndexPage = (props) => (
  <Layout isHome={true}>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <section id="a-propos" className="a-propos">
        <div className="a-propos-wrapper">
          <div className="img-a-propos">
            <Img fluid={props.data.charlesCartoon.childImageSharp.fluid} />
          </div>
          <article>
            <h2>
              Passionné des <br/>technologies de l'information
            </h2> 
            <p>
              D&eacute;veloppeur web habitant la région de Montréal. Passioné par les technologies de l'information, je suis titulaire d'un baccalauréat en Informatique de Gestion à l'Université de Sherbrooke.
              Avec plus de 5 ans d'exp&eacute;rience en développement et gestion de projets web, mes champs d'expertise sont principalement dans le développement web, l'automatisation de tâches et l'optimisation de la vitesse.
            </p>
          </article>
        </div>
      </section>

      <img className="separator" src={separator}/>
      <section id="services" className="section services">
        <h2>Mes champs d'expertise</h2>
        <br/>
        <div className="post-feed">
          <div className="container">
            <div className="service post-card">
              <div className="service-img-wrapper">
                <Img fluid={props.data.imageOne.childImageSharp.fluid} />
              </div>
              <h3>
                D&eacute;veloppement web
              </h3>
              <ul>
                <li>Site vitrine</li>
                <li>Wordpress</li>
                <li>Commerce électronique</li>
                <li>Développement sur mesure</li>
              </ul>
            </div>
          </div>
          <div className="container">
            <div className="service post-card">
              <div className="service-img-wrapper">
                <Img fluid={props.data.imageTwo.childImageSharp.fluid} />
              </div>
              <h3>
                Optimisation
              </h3> 
              <ul>
                <li>Analyse de performance</li>
                <li>Optimisation pour mobile</li>
                <li>Audit de sécurité</li>
                <li>Expérience utilisateur (UI/UX Design)</li>
                <li>CI/CD</li>
              </ul>
            </div>
          </div>
          <div className="container">
            <div className="service post-card">
              <div className="service-img-wrapper">
                <Img fluid={props.data.imageThree.childImageSharp.fluid} />
              </div>
              <h3>
                R&eacute;f&eacute;rencement
              </h3>
              <ul>
                <li>Référencement organique</li>
                <li>Référencement payant</li>
                <li>Inbound marketing</li>
                <li>&nbsp;</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="section technologies">
        <img className="separator" src={separatorWhite}/>
        <h2>Technologies avec lesquelles je travaille</h2>
        <div className="marquee">
          <div className="marquee--inner">
            <span>
              <div className="orb php">
                <Img fluid={props.data.phpImage.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.wordpress.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.shopify.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.nodejs.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.htmlCss.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.reactJs.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <img src={photshopImg} />
              </div>
              <div className="orb">
                <Img fluid={props.data.javascript.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <img src={gatsbyImg} />
              </div>
            </span>
            <span>
              <div className="orb php">
                <Img fluid={props.data.phpImage.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.wordpress.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.shopify.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.nodejs.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.htmlCss.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <Img fluid={props.data.reactJs.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <img src={photshopImg} />
              </div>
              <div className="orb">
                <Img fluid={props.data.javascript.childImageSharp.fluid} />
              </div>
              <div className="orb">
                <img src={gatsbyImg} />
              </div>
            </span>

          </div>
        </div>
      </section>
      <img className="separator" src={separator}/>

      <section id="portfolio" className="portfolio section">
        <h2>Projets auxquels j'ai contribué</h2>
        <br/>
      </section>
      <section className="post-feed">
        <div className="container">
          <a className="post-card" target="_blank" href="https://www.omegawatches.com/">
            <header className="post-card-header">
              <div className="post-card-image">
                <Img height="300" fluid={props.data.omegaPortfolio.childImageSharp.fluid} />
              </div>
              <div className="post-card-tags"> </div>
              <h2 className="post-card-title">Omega Watches</h2>
            </header>
            <section className="post-card-excerpt">
              Site web international de e-commerce traduit en 12 langues pour la multinationale Omega Watches, fabricant de montre suisse.
            </section>
          </a>
        </div>
        <div className="container">
          <a className="post-card" target="_blank" href="https://www.longines.com/">
            <header className="post-card-header">
              <div className="post-card-image">
                <Img height="300" fluid={props.data.longinesPortfolio.childImageSharp.fluid} />
              </div>
              <div className="post-card-tags"> </div>
              <h2 className="post-card-title">Longines</h2>
            </header>
            <section className="post-card-excerpt">
              Site web international développé avec la technologie Magento pour la multinationale Longines, fabricant de montre suisse.
            </section>
          </a>
        </div>
        <div className="container">
          <a className="post-card" target="_blank" href="https://www.bestar.ca">
            <header className="post-card-header">
              <div className="post-card-image">
                <Img height="300" fluid={props.data.bestarPortfolio.childImageSharp.fluid} />
              </div>
              <h2 className="post-card-title">Bestar.ca</h2>
            </header>
            <section className="post-card-excerpt">
              Site e-commerce développé avec Wordpress et Shopify. Synchronisation en temps réel avec stocks et prix de l'ERP.
            </section>
          </a>
        </div>
        <div className="container">
          <a className="post-card" target="_blank" href="https://www.cozey.ca">
            <header className="post-card-header">
              <div className="post-card-image">
                <Img height="300" fluid={props.data.cozeyPortfolio.childImageSharp.fluid} />
              </div>
              <h2 className="post-card-title">Cozey.ca</h2>
            </header>
            <section className="post-card-excerpt">
              Site e-commerce développé avec Wordpress et Shopify. Startup qui commercialise un sofa livr&eacute; dans une bo&icirc;te.
            </section>
          </a>
        </div>
        <div className="container">
          <a className="post-card" target="_blank" href="https://www.onship.ca">
            <header className="post-card-header">
              <div className="post-card-image">
                <Img height="300" fluid={props.data.onshipPortfolio.childImageSharp.fluid} />
              </div>
              <h2 className="post-card-title">OnShip.ca</h2>
            </header>
            <section className="post-card-excerpt">
              E-commerce offrant une vitrine num&eacute;rique aux commer&ccedil;ants ainsi qu'un service de livraison.
            </section>
          </a>
        </div>
        <div className="container">
          <a className="post-card" target="_blank" href="https://www.guidedessoins.com">
            <header className="post-card-header">
              <div className="post-card-image">
                <Img height="300" fluid={props.data.buccoPortfolio.childImageSharp.fluid} />
              </div>
              <div className="post-card-tags"> </div>
              <h2 className="post-card-title">Bücco</h2>
            </header>
            <section className="post-card-excerpt">
              Annuaire de professionnels dentaires géolocalisés au Québec. Prise de rendez-vous en ligne. Outil de marketing destinés aux professionnels de la santé.
            </section>
          </a>
        </div>
      </section>
      <section id="contact" className="section contact">
        <img className="separator" src={separatorWhite}/>
        <h2>Un projet en tête? </h2>
        <h3>514-318-1289</h3>
        <h4><a href="mailto:charles.belzile@hotmail.com">charles.belzile@hotmail.com</a></h4>
        <p>
          Spécialisé dans la conception de site Web, de boutique en ligne et en stratégie web, il me fera de vous accompagnez dans votre projet. Remplissez le formulaire ci-dessous et je tacherai de vous répondre dans les meilleurs délais. 
        </p>
        <br/>
        <form 
        action="https://formspree.io/f/charles.belzile@hotmail.com"
        method="POST" className="contact-form" >
          <b>Votre courriel</b>
          <input type="email" name="email" /> <br/>
          <b>Votre message</b>
          <textarea rows="4" cols="50" name="name" /> <br/>
          <input type="submit" value="Envoyer" />
        </form>
      </section>
  </Layout>
)

export default IndexPage

export const fluidImage = graphql`
fragment fluidImage on File {
  childImageSharp {
    fluid(maxWidth: 1000) {
      ...GatsbyImageSharpFluid
    }
  }
}
`;

export const pageQuery = graphql`
  query {
    charlesCartoon: file(relativePath: { eq: "charles-cartoon.png" }) {
      ...fluidImage
    }
    bestarPortfolio: file(relativePath: { eq: "portfolio/portofolio-bestar-siteweb.png" }) {
      ...fluidImage
    }
    buccoPortfolio: file(relativePath: { eq: "portfolio/portofolio-bucco-siteweb.png" }) {
      ...fluidImage
    }
    longinesPortfolio: file(relativePath: { eq: "portfolio/portfolio-longines-siteweb.png" }) {
      ...fluidImage
    }
    cozeyPortfolio: file(relativePath: { eq: "portfolio/portofolio-cozey-siteweb.png" }) {
      ...fluidImage
    }
    omegaPortfolio: file(relativePath: { eq: "portfolio/portfolio-omega-siteweb.png" }) {
      ...fluidImage
    }
    onshipPortfolio: file(relativePath: { eq: "portfolio/portofolio-onship-siteweb.png" }) {
      ...fluidImage
    }
    imageOne: file(relativePath: { eq: "icon-code.png" }) {
      ...fluidImage
    }
    imageTwo: file(relativePath: { eq: "icon-engrenage.png" }) {
      ...fluidImage
    }
    imageThree: file(relativePath: { eq: "icon-omnichannel.png" }) {
      ...fluidImage
    }
    phpImage: file(relativePath: { eq: "technologies/php.png" }) {
      ...fluidImage
    }
    wordpress: file(relativePath: { eq: "technologies/wordpress.png" }) {
      ...fluidImage
    }
    reactJs: file(relativePath: { eq: "technologies/reactjs.png" }) {
      ...fluidImage
    }
    nodejs: file(relativePath: { eq: "technologies/nodejs.png" }) {
      ...fluidImage
    }
    shopify: file(relativePath: { eq: "technologies/shopify.png" }) {
      ...fluidImage
    }
    htmlCss: file(relativePath: { eq: "technologies/html-css.png" }) {
      ...fluidImage
    }
    javascript: file(relativePath: { eq: "technologies/javascript.png" }) {
      ...fluidImage
    }
    less: file(relativePath: { eq: "technologies/less.png" }) {
      ...fluidImage
    }
  }
`